# Development workflow for the OSCI Pipelines (RHEL-on-GitLab)

Context: OSCI-5828

OSCI already maintains several gating pipelines and we are able to test changes fairly easily, before we deploy them in production.
The current pipelines are in Jenkins and we can simply open a merge request with proposed changes
and a new test pipeline is automatically created and can be used to run tests.
This test pipeline is a copy of the production pipeline with the new changes applied.

There is nothing like this for the RHEL-on-GitLab pipeline at the moment.

Since we are still in the “prototype” stage, we are not very concerned about pipeline breakages.
However, as we are nearing the pilot phase and we will have first real users trying the pipeline,
we need to be more cautious with changes that we push to production.
This document proposes several changes to the structure of the project and the way how to test changes in a production-like environment before they are merged.

## Project structure

Changes in various places (repositories) affect how the production RHEL-on-GitLab pipeline behaves.
Ideally, we should be able to test these changes before they are deployed.
Pipeline definition repository
The RHEL-on-GitLab pipeline definition lives in gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines,
but there are other places where changes can directly or indirectly influence the behavior of the pipeline:

### CI Mediator configuration

This configuration has an effect on the test stages in the pipeline. There is currently no CI (yaml linter) in the repository,
so that is a low hanging fruit for sure.

There is still the problem of testing the changes in the configuration. The fact that the configuration lives
in a separate repository makes things more difficult than they need to be. It should be possible to simply move the configuration to the pipeline definition repository and simplify testing the changes that way.

### Test stage generator script

Part of the RHEL-on-GitLab pipeline is dynamically generated. There is a python script and corresponding jinja2 template
that provides this functionality. However, this script and the template lives in the CI Mediator repository.
The RHEL-on-GitLab pipeline is the only place where we need to use the script, so moving the script and the template
to the pipeline definition repository seems like a logical step that will simplify testing.

### Others

All container images used by the pipeline should be properly versioned and thus updating a container image is a deliberate decision.
We shouldn’t rely on floating image tags that can change unexpectedly, like the `:latest` tag.

There are currently 2 images that use the `:latest` tags:

* images.paas.redhat.com/osci/python:latest
* registry.gitlab.com/redhat/centos-stream/ci-cd/dist-git-gating-tests/check_bz:latest

We should use a specific commit hash and if there is a need to update these images often,
we should invest time and effort in automation. “Renovate” project should be able to help here.

Then there is CI Mediator itself, and the controllers. Changes in both these projects could break the production pipeline.
They are, however, out of scope for this document.

## Development workflow

The assumption is that we moved the CI Mediator configuration and the test stage generator bits to the pipeline definition repository (see the previous section).

* developer opens a MR (mr-1) in the pipeline definition repository
* GitLab CI opens a MR (mr-2) in the osci-internal-test-package repository, adding a dummy .gitlab-ci.yml file which simply includes the pipeline definition from the mr-1:

```yaml
include:
  - remote: 'https://example.com/path/to/your/pipeline.yml'
```

* a comment with a link to the test pipeline in mr-2 is added to the mr-1
* developer checks that the mr-2 pipeline works as expected
* developer merges the mr-1 and thus the changes land in production
* gitLab CI automatically closes the mr-2
