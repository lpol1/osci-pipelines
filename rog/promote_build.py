import os
import koji

if __name__ == "__main__":
    brew_session = koji.ClientSession("https://brewhub.engineering.redhat.com/brewhub")

    CI_COMMIT_SHA = os.environ.get("CI_COMMIT_SHA")
    packageID = os.environ.get("CI_PROJECT_NAME")

    builds = brew_session.listBuilds(
        packageID=packageID,
        state=koji.BUILD_STATES["COMPLETE"],
        source=f"*#{CI_COMMIT_SHA}",
        draft=True,
    )
    build = sorted(builds, key=lambda d: d["completion_ts"], reverse=True)[0]
    print(f"Going to promote build {build['nvr']} completed at {build['completion_time']}.")
    # brew_session.promoteBuild(build["build_id"])
